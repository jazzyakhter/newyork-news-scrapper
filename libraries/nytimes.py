import datetime
import re
import os
import shutil
import time

import requests

from RPA.Browser.Selenium import Selenium
from RPA.Excel.Files import Files
from RPA.HTTP import HTTP
from RPA.Archive import Archive
from SeleniumLibrary.errors import ElementNotFound
from dateutil.relativedelta import relativedelta
from selenium.common import NoSuchElementException
from selenium.webdriver.common.by import By

from libraries.config import OUTPUT, TEMP


class NewYorkTimes:

    def __init__(self, search_text, sections, months):
        self.browser = Selenium()
        self.base_url = 'https://www.nytimes.com/'
        self.news_list = []
        self.search_text = search_text
        self.sections = sections
        if months == 0:
            months = 1
        self.months = months
        self.months = months
        self.files = Files()
        self.http = HTTP()
        self.lib = Archive()

    @staticmethod
    def create_directories():
        if not os.path.exists(TEMP):
            os.mkdir(TEMP)
        if not os.path.exists(OUTPUT):
            os.mkdir(OUTPUT)
        for filename in os.listdir(OUTPUT):
            file_path = os.path.join(OUTPUT, filename)
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)

    def visit_news_times_page(self):
        self.browser.open_available_browser(self.base_url, maximized=True)

    def search(self):
        print(f'Searching with phrase: {self.search_text}')
        self.browser.click_element_if_visible('//button[contains(@class, "css-tkwi90")]')
        self.browser.input_text_when_element_is_visible('//input[@data-testid="search-input"]', self.search_text)
        self.browser.press_keys('//input[@data-testid="search-input"]', 'RETURN')

    def apply_section_filters(self):
        if self.sections:
            self.browser.click_element_when_clickable('//label[text()="Section"]')
            for section in self.sections:
                try:
                    self.browser.click_element_if_visible(f'//label[text()="{section}"]')
                except AssertionError and ElementNotFound:
                    pass

    def sort(self):
        self.browser.wait_until_page_contains_element('//select[@data-testid="SearchForm-sortBy"]')
        self.browser.select_from_list_by_index('//select[@data-testid="SearchForm-sortBy"]', '1')

    def apply_date_filter(self):

        to_date = datetime.datetime.now()
        from_date = to_date - relativedelta(months=self.months)
        to_date = to_date.strftime('%m/%d/%Y')
        from_date = from_date.strftime('%m/%d/%Y')

        print(f'Setting date range from {from_date} to {to_date}.')
        self.browser.click_element_when_visible('//button[@data-testid="search-date-dropdown-a"]')
        self.browser.click_element_when_visible('//button[@aria-label="Specific Dates"]')
        self.browser.input_text_when_element_is_visible('//input[@data-testid="DateRange-startDate"]', from_date)
        self.browser.press_keys('//input[@data-testid="DateRange-endDate"]', to_date + ' RETURN')

    def apply_all_filter(self):
        self.search()
        self.apply_section_filters()
        self.sort()
        self.apply_date_filter()

    def parse_title_and_description(self, news):
        news['contains_any_amount_of_money'] = False
        title_description = news['title'] + news['description']
        pattern = r'\\$\d+\.?\d*|\$\d{1,3}(?:,\d{3})*(?:\.\d{2})?|\b\d+\s+dollars\b|\b\d+\s+USD\b'
        match = re.search(pattern, title_description)
        if match:
            news['contains_any_amount_of_money'] = True

        count = title_description.count(self.search_text)
        news['count'] = count

    def scrap_news(self):
        show_more = True
        while show_more:
            try:
                self.browser.find_element('//button[contains(text(),"Show More")]')
                self.browser.scroll_element_into_view('//button[contains(text(),"Show More")]')
                time.sleep(1)
                self.browser.click_element_when_visible('//button[contains(text(),"Show More")]')
            except AssertionError:
                show_more = False
            except ElementNotFound:
                show_more = False

        for news_element in self.browser.find_elements('//li[@data-testid="search-bodega-result"]'):
            news = {}
            title = news_element.find_element(By.XPATH, './/h4[contains(@class,"css-2fgx4k")]').text
            news['title'] = title
            print(f'Reading news with title {title[:int(len(title)/2)]}....')

            date = news_element.find_element(By.XPATH, './/span[@data-testid="todays-date"]').text
            news['date'] = date

            try:
                description = news_element.find_element(By.XPATH, './/p[@class="css-16nhkrn"]').text
            except NoSuchElementException:
                print(f'Cant read description for news with title {title[:int(len(title)/2)]}......')
                description = ''
            news['description'] = description
            try:
                img_element = news_element.find_element(By.XPATH, './/img')
            except NoSuchElementException:
                print(f'Cant find image for news with title {title[:int(len(title)/2)]}......')
                img_element = None
            if img_element:
                image_url = img_element.get_attribute('src')
                r = requests.get(image_url, allow_redirects=True)
                file_name = f'image-{title[:int(len(title)/2)]}.png'
                file_path = f'{TEMP}/{file_name}'
                open(file_path, 'wb').write(r.content)
            else:
                file_name = ''
            news['file_name'] = file_name
            self.parse_title_and_description(news)

            self.news_list.append(news)

    def start(self):
        print('Started processing news.')
        self.create_directories()
        self.visit_news_times_page()
        self.apply_all_filter()
        try:
            self.browser.wait_until_page_contains_element('//button[contains(text(),"Accept")]')
            self.browser.click_element_when_visible('//button[contains(text(),"Accept")]')
        except Exception as e:
            pass
        time.sleep(2)
        self.scrap_news()

    def finish(self):
        if self.news_list:
            self.files.create_workbook(f'{OUTPUT}/nytimes.xlsx', 'xlsx')
            for news in self.news_list:
                self.files.append_rows_to_worksheet(news, header=True)
            self.files.save_workbook()
            self.files.close_workbook()
            self.lib.archive_folder_with_zip(f'{TEMP}', f'{OUTPUT}/news_images.zip', recursive=True)
        else:
            print('No record found.')
        print('End processing.')
