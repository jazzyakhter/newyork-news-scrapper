from RPA.Robocorp.WorkItems import WorkItems

from libraries.nytimes import NewYorkTimes
from libraries import config


def main():
    try:
        wi = WorkItems()
        wi.get_input_work_item()
        payload = wi.get_work_item_payload()
        search_text = payload.get('search_text')
        sections = payload.get('sections')
        months = payload.get('months')
    except KeyError:
        months = config.months
        sections = config.sections
        search_text = config.search_text
    if not search_text:
        search_text = ''
    if not months:
        months = 0
    nytimes = NewYorkTimes(search_text=search_text, sections=sections, months=months)
    nytimes.start()
    nytimes.finish()


if __name__ == "__main__":
    main()
