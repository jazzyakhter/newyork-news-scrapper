# Fresh News

Challenge code for Fresh News challenge.


## CONFIGRATION

Following variables are configured hard coded in the config.py file but it can be passed as workitems on robocloud process:

- `search_text`: Search news results, must be a string.
- `months`: number of months to capture news within, must be an integer.
- `sections`: List of strings that are valid sections from news sections filter to select while scraping news.


### Test it as python script

1. Start with creating a virtual env:
> python -m venv path_for_venv
2. Install the requirements in the virtual env:
> pip install -r requirements.txt
3. Execute the main file with python:
> python task.py

### Test it using robocloud cli tool

1. Download the RCC module for your operating system from [here](https://github.com/robocorp/rcc#installing-rcc-from-command-line).

2. Open the directory in the terminal.

3. Execute the bot using the following command:
> rcc run

### Run it on robocorp cloud

- First we have to create a robot on the robocloud platform which will contain all the code.

- Create a process with step that execute the robot created previously.

- Execute the process with input values and enter the configration variables to scrap the news.